Soal 1 Membuat Database
create database myshop;

Soal 2 Membuat Table di Dalam Database

Tabel Users
create table users(
    -> id int primary key auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255)
    -> );
Tabel Categories
create table categories(
    -> id int primary key auto_increment,
    -> name varchar(255)
    -> );
Tabel Items
create table items(
    -> id int primary key auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int,
    -> stock int,
    -> category_id int,
    -> foreign key(category_id) references categories(id)
    -> );


Soal 3 Memasukkan Data pada Table

Tabel Users
insert into users(name,email,password) values("John Doe", "john@doe.com", "john123"), ("Jane Doe", "Jane@doe.com", "jenita123");

Tabel Categories
insert into categories(name) values("gadget"), ("cloth"), ("men"), ("women"), ("branded");

Tabel Items
insert into items(name,description,price,stock,category_id) values("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1), ("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2), ("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);


Soal 4 Mengambil Data dari Database

a. Mengambil data users KECUALI password nya.
   select name, email from users;

b. Mengambil data items
      -query untuk mendapatkan data item yang memiliki harga di atas 1000000.
	select * from items where price>1000000;

      -query untuk mengambil data item pada table items yang memiliki name serupa atau mirip (like) dengan kata kunci uniklo
	select * from items where name like 'uniklo%';

c. Menampilkan data items join dengan kategori
   select items.name, items.description, items.price, items.stock, items.category_id, categories.name from items inner join categories on items.category_id = categories.id;

Soal 5 Mengubah Data dari Database
update items set price=2500000 where id=1;